# Contributor: Martijn Braam <martijn@brixit.nl>
# Maintainer: Oliver Smith <ollieparanoid@postmarketos.org>
pkgname=megapixels
pkgver=1.3.2
pkgrel=0
pkgdesc="GTK+4 camera app for mobile devices"
url="https://git.sr.ht/~martijnbraam/megapixels"
# s390x and mips64 blocked by gtk4.0
# riscv64 disabled due to missing rust in recursive dependency
arch="all !s390x !mips64 !riscv64"
license="GPL-3.0-only"
# Required by postprocess.sh
# depends="cmd:dcraw_emu cmd:convert cmd:exiftool"
depends="libraw-tools imagemagick exiftool"
makedepends="glib-dev gtk4.0-dev tiff-dev zbar-dev meson"
source="https://gitlab.com/postmarketOS/megapixels/-/archive/$pkgver/megapixels-$pkgver.tar.bz2"
options="!check" # There's no testsuite

build() {
	abuild-meson . output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
40ec84e254f7b3c019ee96169900731b53e0f3c4d2b7ba3a0cd39af9b6ef155989e890ab1dcbcca99dc80b1feb201a3a431ce738f8e7441833dce0ed272e5c76  megapixels-1.3.2.tar.bz2
"
