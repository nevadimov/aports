# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=spot
pkgver=0.2.1
pkgrel=0
pkgdesc="Native Spotify client for the GNOME desktop"
url="https://github.com/xou816/spot"
arch="aarch64 armhf armv7 ppc64le x86 x86_64"  # limited by rust/cargo
license="MIT"
makedepends="
	alsa-lib-dev
	bash
	cargo
	curl-dev
	glib-dev
	gtk4.0-dev
	libadwaita-dev
	libhandy1-dev
	meson
	nghttp2-dev
	openssl1.1-compat-dev
	pulseaudio-dev
	"
subpackages="$pkgname-lang"
source="https://github.com/xou816/spot/archive/$pkgver/spot-$pkgver.tar.gz
	pulseaudio-backend-only.patch
	"

prepare() {
	default_prepare

	# Optimize binary for size (20 MiB -> 6.1 MiB).
	cat >> Cargo.toml <<-EOF

		[profile.release]
		codegen-units = 1
		lto = true
		opt-level = "z"
		panic = "abort"
	EOF
}

build() {
	# NOTE: buildtype must be release!
	meson \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--buildtype=release \
		-Doffline=false \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	# Meson eats stdout/stderr, so run cargo directly.
	cargo test --locked
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
11230a686a70e3719ddc29e3442efeafdb2e56f074e4c2f8c7595b9cd1e05f0c7874f046662cc5b36ba1adc8476814090c1232038be4a2d91e8dc75478b725ab  spot-0.2.1.tar.gz
85fb88bf9ae703b1f6e30e72e4bde7a823c629e79ecb332f43700ff4aefcf0c1ec1aaa4da7b9f18ba7453ae9e33ee014f16ffdc60aa23828560e92919bd5f19c  pulseaudio-backend-only.patch
"
